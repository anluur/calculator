import React from 'react';
import { Table } from 'react-bootstrap';

const HistoryTable = (props) => {
	return (
		<div
			style={{ height: '250px' }}
			className='d-inline-block overflow-auto w-100 mb-4'
		>
			<Table>
				<thead>
					<tr>
						<th>Operation</th>
						<th>Operand A</th>
						<th>Operand B</th>
						<th>Result</th>
					</tr>
				</thead>
				<tbody>
					{props.history.map((el, idx) => (
						<tr key={idx + el.operation}>
							<td name='operation'>{el.operation}</td>
							<td name='operand1'>{el.operand1}</td>
							<td name='operand2'>{el.operand2}</td>
							<td name='result'>{el.result}</td>
						</tr>
					))}
				</tbody>
			</Table>
		</div>
	);
};
export default HistoryTable;
