import React from 'react';
import { Col } from 'react-bootstrap';
import HistoryTable from './HistoryTable';
import CalculatorForm from './CalculatorForm';

const Calculator = () => {
	const [history, setHistory] = React.useState([]);

	const pushOperationToHistory = (operand1, operand2, operation, result) => {
		const newHistoryElement = {
			operand1,
			operation,
			operand2,
			result,
		};
		setHistory([newHistoryElement, ...history]);
	};

	return (
		<Col sm='8' className='mx-auto'>
			<h1 className='my-4'>Calculator</h1>
			<HistoryTable history={history} />
			<CalculatorForm onSubmit={pushOperationToHistory} />
		</Col>
	);
};

export default Calculator;
