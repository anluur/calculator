import React from 'react';
import { useField } from 'formik';
import { Form } from 'react-bootstrap';

const TextInput = (props) => {
	const { id, ...rest } = props;
	const [field, meta] = useField(props);
	return (
		<Form.Group className='mb-4'>
			<Form.Control {...field} {...rest} />
			{meta.touched && meta.error ? (
				<Form.Text className='text-danger'>{meta.error}</Form.Text>
			) : null}
		</Form.Group>
	);
};

export default TextInput;
