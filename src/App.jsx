import Calculator from './components/Calculator';
import { Container } from 'react-bootstrap';

function App() {
	return (
		<Container>
			<Calculator />
		</Container>
	);
}

export default App;
