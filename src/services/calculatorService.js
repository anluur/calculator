import Big from 'big.js';

function sum(a, b) {
	if (typeof a !== 'number' || typeof b !== 'number') {
		return NaN;
	}
	const x = new Big(a);
	const y = new Big(b);
	return x.plus(y).toNumber();
}

function divide(a, b) {
	if (typeof a !== 'number' || typeof b !== 'number') {
		return NaN;
	}
	if (b === 0) {
		return 'Division by zero';
	}
	const x = new Big(a);
	const y = new Big(b);
	return x.div(y).toNumber();
}

function modulo(a, b) {
	if (typeof a !== 'number' || typeof b !== 'number') {
		return NaN;
	}
	if (b === 0) {
		return 'Division by zero';
	}

	const x = new Big(a);
	const y = new Big(b);
	return x.mod(y).toNumber();
}

function getHighestPrimeNumberBetween(a, b) {
	if (typeof a !== 'number' || typeof b !== 'number') {
		return NaN;
	}
	if (a > b) {
		return 'B < A';
	}
	for (let i = Math.ceil(b) - 1; i > Math.floor(a); i--) {
		if (isPrime(Math.abs(i))) {
			return i;
		}
	}

	return 'Does not exist';
}

function isPrime(num) {
	if (num <= 3) return num > 1;

	if (num % 2 === 0 || num % 3 === 0) return false;

	let count = 5;

	while (Math.pow(count, 2) <= num) {
		if (num % count === 0 || num % (count + 2) === 0) return false;

		count += 6;
	}

	return true;
}

export { sum, divide, modulo, getHighestPrimeNumberBetween };
