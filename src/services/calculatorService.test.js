import {
	sum,
	divide,
	modulo,
	getHighestPrimeNumberBetween,
} from '../services/calculatorService';

describe('sum', () => {
	test('simple sum', () => {
		expect(sum(4, 6)).toBe(10);
	});
	test('fraction sum 1', () => {
		expect(sum(0.25, 3.35)).toBe(3.6);
	});
	test('fraction sum 2', () => {
		expect(sum(0.2, 0.1)).toBe(0.3);
	});
	test('negative sum', () => {
		expect(sum(-6, 4)).toBe(-2);
	});
	test('non-numeric params', () => {
		expect(sum(1, 'a')).toBe(NaN);
	});
});

describe('divide', () => {
	test('division by zero', () => {
		expect(divide(1, 0)).toBe('Division by zero');
	});
	test('simple division 1', () => {
		expect(divide(10, 5)).toBe(2);
	});
	test('simple division 2', () => {
		expect(divide(3, 10)).toBe(0.3);
	});
	test('with fraction', () => {
		expect(divide(0.3, 0.2)).toBe(1.5);
	});
	test('non-numeric params', () => {
		expect(divide(1, 'a')).toBe(NaN);
	});
});

describe('modulo', () => {
	test('division by zero', () => {
		expect(modulo(1, 0)).toBe('Division by zero');
	});
	test('even number', () => {
		expect(modulo(18, 2)).toBe(0);
	});
	test('odd number', () => {
		expect(modulo(51, 2)).toBe(1);
	});
	test('with fractions', () => {
		expect(modulo(91.85, 3.17)).toBe(3.09);
	});
	test('B > A', () => {
		expect(modulo(10, 200)).toBe(10);
	});
	test('non-numeric params', () => {
		expect(modulo(1, 'a')).toBe(NaN);
	});
});

describe('highest prime number between 2 numbers', () => {
	test('simple', () => {
		expect(getHighestPrimeNumberBetween(0, 52)).toBe(47);
	});
	test('with fraction 1', () => {
		expect(getHighestPrimeNumberBetween(0, 52.5)).toBe(47);
	});
	test('with fraction 2', () => {
		expect(getHighestPrimeNumberBetween(1.5, 3)).toBe(2);
	});
	test('not available', () => {
		expect(getHighestPrimeNumberBetween(-2, 2)).toBe('Does not exist');
	});
	test('B < A', () => {
		expect(getHighestPrimeNumberBetween(1000, 50)).toBe('B < A');
	});
	test('with negative numbers 1', () => {
		expect(getHighestPrimeNumberBetween(-25, 3)).toBe(2);
	});
	test('with negative numbers 2', () => {
		expect(getHighestPrimeNumberBetween(-25, -5)).toBe(-7);
	});
	test('non-numeric params', () => {
		expect(getHighestPrimeNumberBetween(1, 'a')).toBe(NaN);
	});
});
