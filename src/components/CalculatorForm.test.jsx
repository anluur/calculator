import { act, cleanup, fireEvent, render } from '@testing-library/react';
import CalculatorForm from './CalculatorForm';

describe('CalculatorForm', () => {
	afterEach(cleanup);
	const changeInput = async (input, value) => {
		await fireEvent.change(input, { target: { value: value } });
		await fireEvent.blur(input);
	};

	const firstOperandPlaceholder = 'Enter first number';
	const secondOperandPlaceholder = 'Enter second number';
	const operationPlaceholder = 'Choose operation';
	const buttonText = 'Calculate';
	const firstOperandErrorMessageText = 'First operand must be a valid number';
	const secondOperandErrorMessageText = 'Second operand must be a valid number';
	const operationErrorMessageText = 'Required';

	describe('with invalid inputs', () => {
		afterEach(cleanup);
		test('renders first operand validation error', async () => {
			const { getByPlaceholderText, queryByText } = render(
				<CalculatorForm onSubmit={() => {}} />
			);

			const operandInput = getByPlaceholderText('Enter first number');
			let errorMessage = null;

			await act(async () => {
				await changeInput(operandInput, 'sss');
			});
			errorMessage = queryByText(firstOperandErrorMessageText);
			expect(errorMessage).not.toBeNull();

			await act(async () => {
				await changeInput(operandInput, '123a');
			});
			errorMessage = queryByText(firstOperandErrorMessageText);
			expect(errorMessage).not.toBeNull();

			await act(async () => {
				await changeInput(operandInput, ' 123');
			});
			errorMessage = queryByText(firstOperandErrorMessageText);
			expect(errorMessage).not.toBeNull();

			await act(async () => {
				await changeInput(operandInput, '12,0');
			});
			errorMessage = queryByText(firstOperandErrorMessageText);
			expect(errorMessage).not.toBeNull();

			await act(async () => {
				await changeInput(operandInput, '');
			});
			errorMessage = queryByText(firstOperandErrorMessageText);
			expect(errorMessage).not.toBeNull();
		});

		test('renders second operand validation error', async () => {
			const { getByPlaceholderText, queryByText } = render(
				<CalculatorForm onSubmit={() => {}} />
			);

			const operandInput = getByPlaceholderText(secondOperandPlaceholder);
			let errorMessage = null;

			await act(async () => {
				await changeInput(operandInput, 'sss');
			});
			errorMessage = queryByText(secondOperandErrorMessageText);
			expect(errorMessage).not.toBeNull();

			await act(async () => {
				await changeInput(operandInput, '123a');
			});
			errorMessage = queryByText(secondOperandErrorMessageText);

			await act(async () => {
				await changeInput(operandInput, ' 123');
			});
			errorMessage = queryByText(secondOperandErrorMessageText);

			expect(errorMessage).not.toBeNull();
			await act(async () => {
				await changeInput(operandInput, '12,0');
			});
			errorMessage = queryByText(secondOperandErrorMessageText);
			expect(errorMessage).not.toBeNull();

			await act(async () => {
				await changeInput(operandInput, '');
			});
			errorMessage = queryByText(secondOperandErrorMessageText);
			expect(errorMessage).not.toBeNull();
		});

		test('renders operation validation error', async () => {
			const { getByPlaceholderText, queryByText } = render(
				<CalculatorForm onSubmit={() => {}} />
			);

			const input = getByPlaceholderText(operationPlaceholder);
			let errorMessage = null;

			await act(async () => {
				await changeInput(input, '');
			});
			errorMessage = queryByText(operationErrorMessageText);
			expect(errorMessage).not.toBeNull();

			await act(async () => {
				await changeInput(input, 'Multiply');
			});
			errorMessage = queryByText(operationErrorMessageText);
			expect(errorMessage).not.toBeNull();
		});

		test('does not trigger form submit action', async () => {
			const onSubmit = jest.fn();
			const { getByPlaceholderText, queryByText } = render(
				<CalculatorForm onSubmit={onSubmit} />
			);

			const firstOperandInput = getByPlaceholderText(firstOperandPlaceholder);
			const secondOperandInput = getByPlaceholderText(secondOperandPlaceholder);
			const operationInput = getByPlaceholderText(operationPlaceholder);
			const button = queryByText(buttonText);

			await act(async () => {
				await changeInput(firstOperandInput, '55');
			});
			await act(async () => {
				await changeInput(secondOperandInput, 'a');
			});
			await act(async () => {
				await changeInput(operationInput, 'Sum');
			});
			await act(async () => {
				await fireEvent.click(button);
			});
			expect(onSubmit).not.toBeCalled();

			await act(async () => {
				await changeInput(firstOperandInput, 'a');
			});
			await act(async () => {
				await changeInput(secondOperandInput, '55');
			});
			await act(async () => {
				await changeInput(operationInput, 'Sum');
			});
			await act(async () => {
				await fireEvent.click(button);
			});
			expect(onSubmit).not.toBeCalled();

			await act(async () => {
				await changeInput(firstOperandInput, '55');
			});
			await act(async () => {
				await changeInput(secondOperandInput, '55');
			});
			await act(async () => {
				await changeInput(operationInput, 'Multiply');
			});
			await act(async () => {
				await fireEvent.click(button);
			});
			expect(onSubmit).not.toBeCalled();
		});
	});

	describe('with valid inputs', () => {
		afterEach(cleanup);
		test('does not render first operand validation error', async () => {
			const { getByPlaceholderText, queryByText } = render(
				<CalculatorForm onSubmit={() => {}} />
			);

			const operandInput = getByPlaceholderText(firstOperandPlaceholder);
			let errorMessage = null;

			await act(async () => {
				await changeInput(operandInput, '1');
			});
			errorMessage = queryByText(firstOperandErrorMessageText);
			expect(errorMessage).toBeNull();

			await act(async () => {
				await changeInput(operandInput, '1.5');
			});
			errorMessage = queryByText(firstOperandErrorMessageText);
			expect(errorMessage).toBeNull();

			await act(async () => {
				await changeInput(operandInput, '.005');
			});
			errorMessage = queryByText(firstOperandErrorMessageText);

			expect(errorMessage).toBeNull();
			await act(async () => {
				await changeInput(operandInput, '-78');
			});
			errorMessage = queryByText(firstOperandErrorMessageText);
			expect(errorMessage).toBeNull();

			await act(async () => {
				await changeInput(operandInput, '-.15');
			});
			errorMessage = queryByText(firstOperandErrorMessageText);
			expect(errorMessage).toBeNull();
		});

		test('does not render second operand validation error', async () => {
			const { getByPlaceholderText, queryByText } = render(
				<CalculatorForm onSubmit={() => {}} />
			);

			const operandInput = getByPlaceholderText(secondOperandPlaceholder);

			let errorMessage = null;

			await act(async () => {
				await changeInput(operandInput, '1');
			});
			errorMessage = queryByText(secondOperandErrorMessageText);
			expect(errorMessage).toBeNull();

			await act(async () => {
				await changeInput(operandInput, '1.5');
			});
			errorMessage = queryByText(secondOperandErrorMessageText);

			await act(async () => {
				await changeInput(operandInput, '.005');
			});
			errorMessage = queryByText(secondOperandErrorMessageText);
			expect(errorMessage).toBeNull();

			await act(async () => {
				await changeInput(operandInput, '-78');
			});
			errorMessage = queryByText(secondOperandErrorMessageText);
			expect(errorMessage).toBeNull();

			await act(async () => {
				await changeInput(operandInput, '-.15');
			});
			errorMessage = queryByText(secondOperandErrorMessageText);
			expect(errorMessage).toBeNull();
		});

		test('does not render operation validation error', async () => {
			const { getByPlaceholderText, queryByText } = render(
				<CalculatorForm onSubmit={() => {}} />
			);

			const input = getByPlaceholderText(operationPlaceholder);
			let errorMessage = queryByText(operationErrorMessageText);

			await act(async () => {
				await changeInput(input, 'Sum');
			});

			expect(errorMessage).toBeNull();

			await act(async () => {
				await changeInput(input, 'Divide');
			});
			expect(errorMessage).toBeNull();

			await act(async () => {
				await changeInput(input, 'Get the remainder of a division');
			});
			expect(errorMessage).toBeNull();

			await act(async () => {
				await changeInput(input, 'Get highest prime number between A and B');
			});
			expect(errorMessage).toBeNull();
		});

		test('triggers form submit action', async () => {
			const onSubmit = jest.fn();
			const { getByPlaceholderText, queryByText } = render(
				<CalculatorForm onSubmit={onSubmit} />
			);

			const firstOperandInput = getByPlaceholderText(firstOperandPlaceholder);
			const secondOperandInput = getByPlaceholderText(secondOperandPlaceholder);
			const operationInput = getByPlaceholderText(operationPlaceholder);
			const button = queryByText(buttonText);

			await act(async () => {
				await changeInput(firstOperandInput, '55.01');
			});
			await act(async () => {
				await changeInput(secondOperandInput, '.12');
			});
			await act(async () => {
				await changeInput(operationInput, 'Sum');
			});
			await act(async () => {
				await fireEvent.click(button);
			});
			expect(onSubmit).toBeCalled();

			await act(async () => {
				await changeInput(firstOperandInput, '-55.01');
			});
			await act(async () => {
				await changeInput(secondOperandInput, '0');
			});
			await act(async () => {
				await changeInput(operationInput, 'Divide');
			});
			await act(async () => {
				await fireEvent.click(button);
			});
			expect(onSubmit).toBeCalled();

			await act(async () => {
				await changeInput(firstOperandInput, '10000');
			});
			await act(async () => {
				await changeInput(secondOperandInput, '-4');
			});
			await act(async () => {
				await changeInput(operationInput, 'Get the remainder of a division');
			});
			await act(async () => {
				await fireEvent.click(button);
			});
			expect(onSubmit).toBeCalled();

			await act(async () => {
				await changeInput(firstOperandInput, '1');
			});
			await act(async () => {
				await changeInput(secondOperandInput, '1');
			});
			await act(async () => {
				await changeInput(
					operationInput,
					'Get highest prime number between A and B'
				);
			});
			expect(onSubmit).toBeCalled();
		});
	});
});
