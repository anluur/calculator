import React from 'react';
import { useField } from 'formik';
import { Form } from 'react-bootstrap';

const SelectInput = (props) => {
	const { id, children, ...rest } = props;
	const [field, meta] = useField(props);
	return (
		<Form.Group controlId={id} className='mb-4'>
			<Form.Control {...field} {...rest} as='select'>
				{children}
			</Form.Control>
			{meta.touched && meta.error ? (
				<Form.Text className='text-danger'>{meta.error}</Form.Text>
			) : null}
		</Form.Group>
	);
};

export default SelectInput;
