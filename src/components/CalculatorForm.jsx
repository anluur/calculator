import React from 'react';
import { Formik, Form } from 'formik';
import * as yup from 'yup';
import { Button } from 'react-bootstrap';
import * as CalculatorService from '../services/calculatorService';
import SelectInput from './shared/SelectInput';
import TextInput from './shared/TextInput';

const operations = new Map([
	['Sum', CalculatorService.sum],
	['Divide', CalculatorService.divide],
	['Get the remainder of a division', CalculatorService.modulo],
	[
		'Get highest prime number between A and B',
		CalculatorService.getHighestPrimeNumberBetween,
	],
]);

const CalculatorForm = (props) => {
	const formValues = {
		operand1: 0,
		operand2: 0,
		operation: 'Sum',
	};

	const validationSchema = yup.object({
		operand1: yup.number().required('First operand must be a valid number'),
		operand2: yup.number().required('Second operand must be a valid number'),
		operation: yup
			.string()
			.required('Required')
			.oneOf([...operations.keys()]),
	});

	return (
		<Formik
			initialValues={formValues}
			validationSchema={validationSchema}
			onSubmit={async (values, { setSubmitting }) => {
				const { operand1, operand2, operation } = values;
				const currentOperation = operations.get(operation);
				const result = currentOperation(
					parseFloat(operand1),
					parseFloat(operand2)
				);
				props.onSubmit(operand1, operand2, operation, result);
				setSubmitting(false);
			}}
		>
			<Form>
				<TextInput
					id='operand1'
					name='operand1'
					placeholder='Enter first number'
					type='number'
				/>
				<SelectInput
					id='operation'
					name='operation'
					placeholder='Choose operation'
				>
					{[...operations.keys()].map((opName) => (
						<option key={opName} value={opName}>
							{opName}
						</option>
					))}
				</SelectInput>
				<TextInput
					id='operand2'
					name='operand2'
					placeholder='Enter second number'
					type='number'
				/>
				<Button type='submit'>Calculate</Button>
			</Form>
		</Formik>
	);
};

export default CalculatorForm;
