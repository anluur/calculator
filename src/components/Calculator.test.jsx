import { act, cleanup, fireEvent, render } from '@testing-library/react';
import Calculator from './Calculator';

describe('Calculator', () => {
	afterEach(cleanup);
	const changeInput = async (input, value) => {
		await fireEvent.change(input, { target: { value: value } });
		await fireEvent.blur(input);
	};

	const firstOperandPlaceholder = 'Enter first number';
	const secondOperandPlaceholder = 'Enter second number';
	const operationPlaceholder = 'Choose operation';
	const buttonText = 'Calculate';

	describe('history', () => {
		afterEach(cleanup);
		test('calculation result is in history table', async () => {
			const { getByPlaceholderText, queryByText, container } = render(
				<Calculator />
			);

			const firstOperandInput = getByPlaceholderText(firstOperandPlaceholder);
			const secondOperandInput = getByPlaceholderText(secondOperandPlaceholder);
			const operationInput = getByPlaceholderText(operationPlaceholder);
			const button = queryByText(buttonText);

			const firstOperandValue = '2';
			const secondOperandValue = '3';
			const operationValue = 'Sum';

			await act(async () => {
				await changeInput(firstOperandInput, firstOperandValue);
			});
			await act(async () => {
				await changeInput(secondOperandInput, secondOperandValue);
			});
			await act(async () => {
				await changeInput(operationInput, operationValue);
			});
			await act(async () => {
				await fireEvent.click(button);
			});

			const firstTableRow = container.querySelector(
				'table tbody tr:first-child'
			);
			expect(firstTableRow).not.toBeNull();
			const operation = firstTableRow.querySelector('td[name="operation"]');
			const operand1 = firstTableRow.querySelector('td[name="operand1"]');
			const operand2 = firstTableRow.querySelector('td[name="operand2"]');
			const result = firstTableRow.querySelector('td[name="result"]');
			expect(operand1.innerHTML).toEqual(firstOperandValue);
			expect(operand2.innerHTML).toEqual(secondOperandValue);
			expect(operation.innerHTML).toEqual(operationValue);
			expect(result.innerHTML).toEqual('5');
		});

		test('calculation result remains in history table', async () => {
			const { getByPlaceholderText, queryByText, container } = render(
				<Calculator />
			);

			const firstOperandInput = getByPlaceholderText(firstOperandPlaceholder);
			const secondOperandInput = getByPlaceholderText(secondOperandPlaceholder);
			const operationInput = getByPlaceholderText(operationPlaceholder);
			const button = queryByText(buttonText);

			const firstOperandValue = '2';
			const secondOperandValue = '3';
			const operationValue = 'Sum';

			// Do first calculation
			await act(async () => {
				await changeInput(firstOperandInput, firstOperandValue);
			});
			await act(async () => {
				await changeInput(secondOperandInput, secondOperandValue);
			});
			await act(async () => {
				await changeInput(operationInput, operationValue);
			});
			await act(async () => {
				await fireEvent.click(button);
			});

			// Do second calculation with flipped values
			await act(async () => {
				await changeInput(firstOperandInput, secondOperandValue);
			});
			await act(async () => {
				await changeInput(secondOperandInput, firstOperandValue);
			});
			await act(async () => {
				await changeInput(operationInput, operationValue);
			});
			await act(async () => {
				await fireEvent.click(button);
			});

			// Second calculation is on top
			const firstTableRow = container.querySelector(
				'table tbody tr:first-child'
			);
			expect(firstTableRow).not.toBeNull();
			const firstOperation = firstTableRow.querySelector(
				'td[name="operation"]'
			);
			const firstOperand1 = firstTableRow.querySelector('td[name="operand1"]');
			const firstOperand2 = firstTableRow.querySelector('td[name="operand2"]');
			const firstResult = firstTableRow.querySelector('td[name="result"]');
			expect(firstOperand1.innerHTML).toEqual(secondOperandValue);
			expect(firstOperand2.innerHTML).toEqual(firstOperandValue);
			expect(firstOperation.innerHTML).toEqual(operationValue);
			expect(firstResult.innerHTML).toEqual('5');

			// First calculation is in the next row
			const lastTableRow = container.querySelector('table tbody tr:last-child');
			expect(lastTableRow).not.toBeNull();
			const lastOperation = lastTableRow.querySelector('td[name="operation"]');
			const lastOperand1 = lastTableRow.querySelector('td[name="operand1"]');
			const lastOperand2 = lastTableRow.querySelector('td[name="operand2"]');
			const lastResult = lastTableRow.querySelector('td[name="result"]');
			expect(lastOperand1.innerHTML).toEqual(firstOperandValue);
			expect(lastOperand2.innerHTML).toEqual(secondOperandValue);
			expect(lastOperation.innerHTML).toEqual(operationValue);
			expect(lastResult.innerHTML).toEqual('5');
		});
	});
});
